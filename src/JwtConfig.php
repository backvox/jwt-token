<?php

declare(strict_types=1);

namespace Voxonics\Jwt;

use Illuminate\Support\Str;
use \RuntimeException;
use Ramsey\Uuid\Uuid;

/**
 * Class JwtConfig
 *
 * Basic config for Jwt token
 *
 * @see https://jwt.io/introduction/
 */
class JwtConfig
{
    /**
     * @var array
     */
    protected $config;

    /**
     * Type of token (typ)
     *
     * @var string
     */
    protected $type;

    /**
     * Signature algorithm (alg)
     *
     * @var string
     */
    protected $algorithm;

    /**
     * Who created token (iss)
     *
     * @var string
     */
    protected $issuer;

    /**
     * Token intended for (aud)
     *
     * @var string
     */
    protected $audience;

    /**
     * Token created time (iat)
     *
     * @var int
     */
    protected $issuedAt;

    /**
     * Token time to live (ttl)
     *
     * @var int
     */
    protected $timeToLive;

    /**
     * Can only be used after time (nbf)
     *
     * @var int
     */
    protected $canOnlyBeUsedAfter;

    /**
     * Not valid after (exp)
     *
     * @var int
     */
    protected $expiredAt;

    /**
     * Token unique identifier (jti)
     *
     * @var string
     */
    protected $tokenUid;

    /**
     * Private key to generate token signature
     *
     * @var string
     */
    protected $key;

    /**
     * JwtConfig constructor.
     *
     * @var array $config
     */
    public function __construct(array $config = [])
    {
        $this->config = $config;

        $this->init($config);
    }

    /**
     * Base config for Jwt Auth
     *
     * @var array $config
     */
    protected function init(array $config): void
    {
        $this
            ->setType($config['type'] ?? $_SERVER['JWT_TYPE'] ?? 'JWT')
            ->setAlgorithm($config['alg'] ?? $_SERVER['JWT_ALG'] ?? 'HS256')
            ->setIssuer($config['iss'] ?? $_SERVER['JWT_ISS'] ?? '')
            ->setAudience($config['aud'] ?? $_SERVER['JWT_AUD'] ?? '')
            ->setIssuedAt($config['issued'] ?? $_SERVER['JWT_ISSUED_AT'] ?? null)
            ->setTimeToLive($config['ttl'] ?? $_SERVER['JWT_TTL'] ?? null)
            ->setCanOnlyBeUsedAfter($config['ndf'] ?? $_SERVER['JWT_NBF'] ?? null)
            ->setExpiredAt($config['exp'] ?? $_SERVER['JWT_EXP'] ?? null)
            ->setKey($config['key'] ?? $_SERVER['JWT_SECRET_KEY'] ?? '')
            ->setTokenUid($config['jti'] ?? null);
    }

    /**
     * Get new JwtConfig instance
     *
     * @param array $config
     *
     * @return static
     */
    public function newInstance(array $config = []): self
    {
        return new static($config);
    }

    /**
     * Refresh current config data.
     *
     * @param array $config
     *
     * @return static
     */
    public function refresh(array $config = []): self
    {
        $this->init($config);

        return $this;
    }

    /**
     * Get config data
     *
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return static
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlgorithm(): string
    {
        return $this->algorithm;
    }

    /**
     * @param string $algorithm
     *
     * @return static
     */
    public function setAlgorithm(string $algorithm): self
    {
        $this->algorithm = $algorithm;

        return $this;
    }

    /**
     * @return string
     */
    public function getIssuer(): string
    {
        return $this->issuer;
    }

    /**
     * @param string $issuer
     *
     * @return static
     */
    public function setIssuer(string $issuer): self
    {
        $this->issuer = $issuer;

        return $this;
    }

    /**
     * @return string
     */
    public function getAudience(): string
    {
        return $this->audience;
    }

    /**
     * @param string $audience
     *
     * @return static
     */
    public function setAudience(string $audience): self
    {
        $this->audience = $audience;

        return $this;
    }

    /**
     * @return int
     */
    public function getIssuedAt(): int
    {
        return $this->issuedAt;
    }

    /**
     * @param int|null $issuedAt
     *
     * @return static
     */
    public function setIssuedAt(int $issuedAt = null): self
    {
        $issuedAt = $issuedAt ?: \time();

        $this->issuedAt = $issuedAt;

        return $this;
    }

    /**
     * @return int
     */
    public function getTimeToLive(): int
    {
        return $this->timeToLive;
    }

    /**
     * @param int|null $timeToLive
     *
     * @return static
     */
    public function setTimeToLive(int $timeToLive = null): self
    {
        $timeToLive = $timeToLive ?: 86400;

        $this->timeToLive = $timeToLive;

        return $this;
    }

    /**
     * @return int
     */
    public function getCanOnlyBeUsedAfter(): int
    {
        return $this->canOnlyBeUsedAfter ?? $this->getIssuedAt();
    }

    /**
     * @param int|null $canOnlyBeUsedAfter
     *
     * @return static
     */
    public function setCanOnlyBeUsedAfter(int $canOnlyBeUsedAfter = null): self
    {
        if ($canOnlyBeUsedAfter !== null) {
            $this->canOnlyBeUsedAfter = $canOnlyBeUsedAfter;
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getExpiredAt(): int
    {
        return $this->expiredAt ?? $this->getIssuedAt() + $this->getTimeToLive();
    }

    /**
     * @param int|null $expiredAt
     *
     * @return static
     */
    public function setExpiredAt(int $expiredAt = null): self
    {
        if ($expiredAt !== null) {
            $this->expiredAt = $expiredAt;
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTokenUid(): ?string
    {
        return $this->tokenUid;
    }

    /**
     * @param string|null $tokenUid
     *
     * @return static
     */
    public function setTokenUid(string $tokenUid = null): self
    {
        $this->tokenUid = $tokenUid ?? (string) Str::uuid();

        return $this;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $key
     *
     * @return static
     */
    public function setKey(string $key): self
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Handle dynamic static method calls.
     *
     * @param  string   $method
     * @param  array    $parameters
     *
     * @return mixed
     */
    public static function __callStatic($method, $parameters)
    {
        return (new static)->$method(...$parameters);
    }
}
