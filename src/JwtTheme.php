<?php

declare(strict_types=1);

namespace Voxonics\Jwt;

use JsonSerializable;
use Lcobucci\JWT\Token;
use Voxonics\Jwt\Exceptions\InvalidAlgorithmException;
use OutOfBoundsException;

/**
 * Class JwtTheme
 */
final class JwtTheme implements JsonSerializable
{
    /**
     * @var JwtService
     */
    private $jwtService;

    /**
     * Jwt Auth token
     *
     * @var Token|null
     */
    protected $token;

    /**
     * JwtTheme constructor.
     *
     * @param JwtService $jwtService
     */
    public function __construct(JwtService $jwtService)
    {
        $this->jwtService = $jwtService;
    }

    /**
     * @return JwtService
     */
    public function getJwtService(): JwtService
    {
        return $this->jwtService;
    }
    
    /**
     * Create token
     *
     * @param array         $payload token payload data
     * @param int           $ttl token time to live
     * @param string|null   $jti token unique identifier
     * @param string|null   $secretKey token secret key
     *
     * @return static
     *
     * @throws InvalidAlgorithmException
     */
    public function create(array $payload, int $ttl = 0, string $jti = null, string $secretKey = null): self
    {
        return $this->setToken(
            $this->jwtService->generateToken($payload, $ttl, $jti, $secretKey)
        );
    }

    /**
     * Validate current token
     *
     * @return bool
     */
    public function validate(): bool
    {
        return !($this->token === null) && $this->jwtService->validate($this->token);
    }

    /**
     * Validate current token signature
     *
     * @return bool
     */
    public function verify(): bool
    {
        return !($this->token === null) && $this->jwtService->verify($this->token);
    }

    /**
     * @return Token|null
     */
    public function getToken(): ?Token
    {
        return $this->token;
    }

    /**
     * @param Token|null    $token
     *
     * @return static
     */
    public function setToken(?Token $token = null): self
    {
        if ($token !== null) {
            $this->token = $token;
        }

        return $this;
    }

    /**
     * Parse given bearer
     *
     * @param string $bearer
     *
     * @return Token
     */
    public function parse(string $bearer): Token
    {
        $token = $this->jwtService->parse($bearer);

        $this->setToken($token);

        return $token;
    }

    /**
     * Returns the value of a current token claim
     *
     * @param string        $name
     * @param mixed|null    $default
     *
     * @return mixed
     *
     * @throws OutOfBoundsException
     */
    public function getClaim(string $name, $default = null)
    {
        return $this->token->getClaim($name, $default);
    }

    /**
     * {@inheritDoc}
     *
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [];

        if ($this->token !== null) {
            $data['token'] = (string) $this->token;
        }

        return $data;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->token;
    }
}
