<?php

declare(strict_types=1);

namespace Voxonics\Jwt\Exceptions;

use Exception;

/**
 * Class InvalidAlgorithmException
 */
class InvalidAlgorithmException extends Exception
{
}
